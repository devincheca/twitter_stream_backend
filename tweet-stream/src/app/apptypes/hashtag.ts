export interface hashtag {
  hashtagText: string;
  hashtagCount: number;
  timeStamp: Date;
  entryTotal: number;
}
