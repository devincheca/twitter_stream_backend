import { Component, OnInit } from '@angular/core';
import { hashtag, stream } from './apptypes/apptypes';
import getStream from './helpers/getStream';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  countArray: number[] = [];
  hashtagArray: hashtag[] = [];
  isLoading: boolean = true;

  hideLoader(): void {
    this.isLoading = false;
    this.setBodyColor();
  }

  setBodyColor(): void {
    Array.from(document.getElementsByTagName('body'))
      .map(body => body.style.backgroundColor = 'white');
  }

  async stream(): Promise<void> {
    const res: stream = await getStream();
    this.hashtagArray = [...this.hashtagArray, ...res.topHashtags];
    this.countArray = [...this.countArray, res.count];
    setTimeout(() => { this.stream(); }, 8000);
  }

  ngOnInit(): void {
    this.stream();
  }
}
