export interface calculatedHashtag {
  hashtagText: string;
  hashtagCount: number;
  timeStamp: Date;
  hashtagPercent: string;
}
