import { MediaMatcher } from '@angular/cdk/layout';
import { ChangeDetectorRef, Input, Component, OnDestroy, OnInit } from '@angular/core';
import { calculatedHashtag, hashtag } from '../apptypes/apptypes';

@Component({
  selector: 'app-main-view',
  templateUrl: './main-view.component.html',
  styleUrls: ['./main-view.component.css'],
})
export class MainViewComponent implements OnDestroy, OnInit {
  @Input() countArray: number[] = [];
  @Input() hashtagArray: hashtag[] = [];

  isTooThin: boolean = window.screen.availWidth <= 280;
  mobileQuery: MediaQueryList;

  fillerNav: string[] = [];
  navSet: Set<string> = new Set();
  topTimeStamp: Date = new Date();
  selectedHashtag: string = '';

  private _mobileQueryListener: () => void;

  constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  navToHashtag(nav?: string): void {
    if (nav) {
      this.selectedHashtag = nav;
    } else {
      this.selectedHashtag = '';
    }
    this.toggleSideNav();
  }
  
  toggleSideNav(): void {
    document.getElementById('main_view_burger_button')?.click();
  }

  selectedHashtagArray(): calculatedHashtag[] {
    if (this.selectedHashtag) {
      return this.calcPercentage(this.hashtagArray.filter(tag => tag.hashtagText.includes(this.selectedHashtag)));
    }
    return this.calcPercentage(this.hashtagArray);
  }

  calcPercentage(arrayToCalc: hashtag[]): calculatedHashtag[] {
    return arrayToCalc.map(tag => {
      return Object.assign({}, tag, {
        hashtagPercent: (tag.hashtagCount / tag.entryTotal).toFixed(2)
      });
    })
  }

  getTimeFormat(tag: calculatedHashtag): string {
    const time = tag.timeStamp;
    return `${time.getMonth() + 1}/${time.getDay()}/${time.getFullYear()} ${time.getHours()}:${time.getMinutes()}:${time.getSeconds()}`;
  }

  getContentClass(i: number) {
    return i % 2 === 0 ? 'even-div' : 'odd-div';
  }

  getCountHeader(): string {
    return this.countArray[this.countArray.length - 1]
      .toString()
      .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  getDateText(): string {
    return `${this.topTimeStamp.getMonth() + 1}/${this.topTimeStamp.getDay()}/${this.topTimeStamp.getFullYear()}`;
  }

  getDateHoursAndMinutes(): string {
    return `${this.topTimeStamp.getHours()}:${this.topTimeStamp.getMinutes()}`;
  }

  getTotalTweetArray(): number[] {
    return this.hashtagArray.map((tag: hashtag, i: number) => {
      return tag.hashtagCount;
    });
  }

  refreshNav(): void {
    this.hashtagArray.forEach((tag: hashtag) => {
      this.navSet.add(tag.hashtagText);
    });
    this.fillerNav = Array.from(this.navSet);
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  ngOnInit(): void {
    this.refreshNav();
  }
}