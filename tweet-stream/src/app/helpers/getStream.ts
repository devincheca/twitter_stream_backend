import { stream } from '../apptypes/apptypes';

export default async function getStream(): Promise<stream> {
  try {
    const res = await fetch('https://tweet-stream.ti-manager.com/api/v1/tweet-stats');
    const json = await res.json();
    const currentTotal = json.topHashtags
      .map((tag: any[]) => tag[1])
      .reduce((count: number, total: number) => total + count, 0);
    return {
      count: json.count,
      topHashtags: json.topHashtags.map((tag: any[]) => {
        return {
          hashtagText: tag[0],
          hashtagCount: tag[1],
          timeStamp: new Date(),
          entryTotal: currentTotal
        };
      })
    };
  } catch(error) {
    console.trace(error);
    setTimeout(getStream, 2000);
    return {
      count: 0,
      topHashtags: [] 
    };
  }
}
