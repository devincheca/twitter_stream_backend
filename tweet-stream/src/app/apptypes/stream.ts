import { hashtag } from './hashtag';
export interface stream {
  count: number;
  topHashtags: hashtag[];
}
