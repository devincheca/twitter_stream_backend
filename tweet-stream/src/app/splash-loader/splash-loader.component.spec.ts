import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SplashLoaderComponent } from './splash-loader.component';

describe('SplashLoaderComponent', () => {
  let component: SplashLoaderComponent;
  let fixture: ComponentFixture<SplashLoaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SplashLoaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SplashLoaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should animate', async () => {
    const fixture = TestBed.createComponent(SplashLoaderComponent);
    const splash = fixture.componentInstance;
    expect(splash.loadingText).toEqual('Loading');
  });

  it('should render twitter logo', () => {
    const fixture = TestBed.createComponent(SplashLoaderComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.fa.fa-twitter').textContent.length).toEqual(0);
  });
});
