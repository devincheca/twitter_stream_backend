import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-splash-loader',
  templateUrl: './splash-loader.component.html',
  styleUrls: ['./splash-loader.component.css']
})
export class SplashLoaderComponent implements OnInit {

  constructor() { }

  loadingText: string = 'Loading';

  @Output() loaderComplete = new EventEmitter<void>();

  loopDotDotDot():void {
    setTimeout(() => {
      this.loadingText = this.loadingText.includes('. . .')
        ? 'Loading'
        : this.loadingText + ' .';
        this.loopDotDotDot();
    }, 400);
  }

  ngOnInit(): void {
    setTimeout(() => { this.loaderComplete.emit() }, 1500);
    this.loopDotDotDot();
  }

}
